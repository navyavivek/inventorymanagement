public class item {
        private double costPrice;
        private double sellingPrice;
        private String itemName;
        private int quantity;


        public item(double costPrice, double sellingPrice, String itemName, int quantity) {
            this.costPrice = costPrice;
            this.sellingPrice = sellingPrice;
            this.itemName = itemName;
            this.quantity = quantity;
        }

        public double getCostPrice() {
            return costPrice;
        }

        public void setCostPrice(double costPrice) {
            this.costPrice = costPrice;
        }

        public double getSellingPrice() {
            return sellingPrice;
        }

        public void setSellingPrice(double sellingPrice) {
            this.sellingPrice = sellingPrice;
        }

        public String getItemName() {
            return itemName;
        }

        public void setItemName(String itemName) {
            this.itemName = itemName;
        }

        public int getQuantity() {
            return quantity;
        }

        public void setQuantity(int quantity) {
            this.quantity = quantity;
        }
    }


