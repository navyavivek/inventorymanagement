import java.util.ArrayList;
import java.util.List;

public class transactionManagement {
    public static final transactionManagement instance2 = new transactionManagement();
    private transactionManagement(){}
    public static transactionManagement getInstance()
    {
        return instance2;
    }

    List<Transaction> transactionList = new ArrayList<>();

    public Double getProfit()
    {
        Double profit = 0.00;
        for(Transaction k : transactionList)
        {
            profit = profit + k.getProfit();
        }
        return profit;
    }

    public void addTransaction(String itemName, String command, Double BroughtAt, Double SoldAt, int quantity)
    {
        Double profitTotal = (SoldAt - BroughtAt) * quantity;
        Transaction transactionObj = new Transaction(itemName,command,profitTotal,BroughtAt,SoldAt,quantity);
        transactionList.add(transactionObj);
    }
    public void clearTransactionManagement(){
        transactionList.clear();
    }
}
