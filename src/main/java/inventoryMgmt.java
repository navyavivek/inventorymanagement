import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class inventoryMgmt {
    Map <String, item> itemList = new TreeMap <String, item>();
    public static final inventoryMgmt instance = new inventoryMgmt();
    private inventoryMgmt(){}
    public static inventoryMgmt getInstance()
    {
        return instance;
    }
    transactionManagement transactionManagementObj = transactionManagement.getInstance();
    public void createItem(String itemName, Double costPrice, Double sellingPrice)
    {
        if(itemList.containsKey(itemName))
        {
            System.out.println("The item is already present in inventory");
        }
        else
        {
            item itemObj = new item(costPrice,sellingPrice,itemName,0);
            itemList.put(itemName,itemObj);
        }
    }

    public void deleteItem(String itemName) {
        if(itemList.containsKey(itemName))
        {
            item itemObj = itemList.get(itemName);
            itemList.remove(itemName);
            transactionManagementObj.addTransaction(itemName,"deleteItem",itemObj.getCostPrice(),0.00,itemObj.getQuantity());
            System.out.println("The item is no longer present");
        }
        else
        {
            System.out.println("The item not in inventory");
        }

    }
    public void updateSell(String itemName, int itemQuantity) {
        item itemObj = itemList.get(itemName);
        int itemOldQuantity = itemObj.getQuantity();
        if(itemQuantity > itemOldQuantity)
        {
            System.out.println("There is not enough items");
        }
        int newItemQuantity = itemOldQuantity - itemQuantity;
        if(newItemQuantity == 0)
        {
            itemList.remove(itemName);
        }
        itemObj.setQuantity(newItemQuantity);
        transactionManagementObj.addTransaction(itemName,"updateSell",itemObj.getCostPrice(),itemObj.getSellingPrice(),itemQuantity);
    }

    public void updateBuy(String itemName, int itemQuantity) {
        item itemObj = itemList.get(itemName);
        int itemOldQuantity = itemObj.getQuantity();
        int newItemQuantity = itemOldQuantity + itemQuantity;
        itemObj.setQuantity(newItemQuantity);
        //transactionManagementObj.addTransaction(itemName,"updateSell",itemObj.getCostPrice(),itemObj.getSellingPrice(),itemQuantity);
    }

    public void updateSellingPrice(String itemName, Double newSellingPrice) {
        item itemObj = itemList.get(itemName);
        itemObj.setSellingPrice(newSellingPrice);
    }
    public void report() {
        Set <String> keys = itemList.keySet();
        Double totalValue = 0.00;
        System.out.println("Inventory Report");
        System.out.println("ItemName \t BroughtAt \t SoldAt \t AvailableQty \t Value \t");
        System.out.println("-------- \t -------- \t ------- \t ----------- \t ------ \t");
        for (String key : keys) {
            item itemObj = itemList.get(key);
            Double value = itemObj.getCostPrice() * itemObj.getQuantity();
            System.out.printf("%s  \t   %.02f    \t   %.02f   \t   %d    \t    %.02f  \t  \n",itemObj.getItemName(),itemObj.getCostPrice(),itemObj.getSellingPrice(),itemObj.getQuantity(),value);
            totalValue = totalValue+ value;

        }
        totalValue = Math.ceil(totalValue);
        System.out.println("--------------------------------------------------------------------------------------------------------------------------------------------");
        System.out.println("TotalValue                                                                                              " + totalValue);
        System.out.println("Profit since previous report                                                                                     " + transactionManagementObj.getProfit());
        transactionManagementObj.clearTransactionManagement();
    }
}
