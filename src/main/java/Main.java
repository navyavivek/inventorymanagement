import java.util.Scanner;

public class Main {
    public static void main(String args[]) {
        while (true) {
            System.out.println("Please enter your command: ");
            Scanner console = new Scanner(System.in);
            String command = console.nextLine();
            String[] stringArray = command.split(" ");
            inventoryMgmt inventory = inventoryMgmt.getInstance ();
            System.out.println(stringArray[0]);
            switch (stringArray[0]) {
                case "create": {
                    if (stringArray.length == 4) {
                        Double costPrice = Math.ceil(Double.parseDouble(stringArray[2]));
                        Double sellingPrice = Math.ceil(Double.parseDouble(stringArray[3]));
                        inventory.createItem(stringArray[1], costPrice, sellingPrice);
                    }
                    break;
                }
                case "deleteItem": {
                     if (stringArray.length == 2) {
                    inventory.deleteItem(stringArray[1]);
                     }
                     break;
                }
                case "updateSell": {
                    if (stringArray.length == 3) {
                        int itemQuantity = Integer.parseInt(stringArray[2]);
                        inventory.updateSell(stringArray[1], itemQuantity);
                    }
                        break;

                }
                case "updateBuy": {
                    if (stringArray.length == 3) {
                        int itemQuantity = Integer.parseInt(stringArray[2]);
                        inventory.updateBuy(stringArray[1], itemQuantity);
                    }
                        break;

                }
                case "updateSellingPrice": {
                    if (stringArray.length == 3) {
                        Double newSellingPrice = Double.parseDouble(stringArray[2]);
                        inventory.updateSellingPrice(stringArray[1], newSellingPrice);
                    }
                        break;

                }
                case "report": {
                    inventory.report();
                    break;
                }
                default:
                    System.out.println("def");
                    break;
            }

        }
    }
}
