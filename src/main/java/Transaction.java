public class Transaction {
    private String itemName;
    private String command;
    private Double profit;
    private Double BroughtAt;
    private Double SoldAt;
    private int Quantity;

    public Transaction(String itemName, String command, Double profit, Double BroughtAt, Double SoldAt, int quantity) {
        this.itemName = itemName;
        this.command = command;
        this.profit = profit;
        BroughtAt = BroughtAt;
        SoldAt = SoldAt;
        Quantity = quantity;
    }


    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public Double getProfit() {
        return profit;
    }

    public void setProfit(Double profit) {
        this.profit = profit;
    }

    public Double getBroughtAt() {
        return BroughtAt;
    }

    public void setBroughtAt(Double boughtAt) {
        BroughtAt = boughtAt;
    }

    public Double getSoldAt() {
        return SoldAt;
    }

    public void setSoldAt(Double soldAt) {
        SoldAt = SoldAt;
    }

    public int getQuantity() {
        return Quantity;
    }

    public void setQuantity(int quantity) {
        Quantity = quantity;
    }

}
