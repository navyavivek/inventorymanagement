import org.junit.Test;

import static junit.framework.TestCase.assertTrue;

public class itemTest {
    @Test
    public void setcostPriceTest()
    {
        item itemObj = new item(10,40,"Book",8);
        itemObj.setCostPrice(10);
        assertTrue(itemObj.getCostPrice() == 10);
    }
    @Test
    public void getcostPriceTest()
    {
        item itemObj = new item(20,60,"Book",10);
        itemObj.setCostPrice(20);
        assertTrue(itemObj.getCostPrice() == 20);
    }
    @Test
    public void setSellingPriceTest()
    {
        item itemObj = new item(10,20,"Book",8);
        itemObj.setSellingPrice(40);
        assertTrue(itemObj.getSellingPrice() == 40);
    }
    @Test
    public void getSellingPriceTest()
    {
        item itemObj = new item(20,60,"Book",10);
        itemObj.setSellingPrice(20);
        assertTrue(itemObj.getSellingPrice() == 20);
    }
    @Test
    public void setquantityTest()
    {
        item itemObj = new item(10,20,"Book",8);
        itemObj.setQuantity(5);
        assertTrue(itemObj.getQuantity() == 5);
    }
    @Test
    public void getquantityTest()
    {
        item itemObj = new item(20,60,"Book",10);
        itemObj.setSellingPrice(10);
        assertTrue(itemObj.getSellingPrice() == 10);
    }
    @Test
    public void setitemNameTest()
    {
        item itemObj = new item(10,20,"Book",8);
        itemObj.setItemName("Book");
        assertTrue(itemObj.getItemName() == "Book");
    }
    @Test
    public void getitemNameTest()
    {
        item itemObj = new item(20,60,"Book",10);
        itemObj.setItemName("Book");
        assertTrue(itemObj.getItemName() == "Book");
    }





}
