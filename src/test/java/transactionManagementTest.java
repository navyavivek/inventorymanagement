import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class transactionManagementTest {
    @Test
    public void getProfitTest() {
        transactionManagement transactionManagementObj= transactionManagement.getInstance();
        transactionManagementObj.addTransaction("Book","deleteItem",10.00,50.00,5);
        double profit = Math.ceil((50.00 - 10.00) * 5);
        assertEquals(profit,transactionManagementObj.getProfit());
    }
    @Test
    public void getProfitTest1() {
        transactionManagement transactionManagementObj= transactionManagement.getInstance();
        transactionManagementObj.addTransaction("Book","updateSell",10.00,50.00,5);
        double profit = (50.00 - 10.00) * 5;
        assertEquals(profit,transactionManagementObj.getProfit());
    }
}
